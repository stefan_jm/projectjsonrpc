﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectJSONRPCValidate.view.Models;
using ProjectJSONRPCValidate.wrapper.Core.Data.Defect;

namespace ProjectJSONRPCValidate.view.Pages
{
    public class IndexModel : PageModel
    {
        // Remote procedure call wrapper
        private readonly IDefectWrapper _defectWrapper;


        public IndexModel(IDefectWrapper defectWrapper)
        {
            _defectWrapper = defectWrapper;
        }

        public ResultMessage Message { get; set; }

        public class ResultMessage
        {
            public bool IsError { get; set; }
            public string Message { get; set; }

            public ResultMessage(string message, bool isError)
            {
                IsError = isError;
                Message = message;
            }
        }

        [BindProperty]
        public DefectModelVM Defect { get; set; } = new DefectModelVM();

        public List<DefectModelVM> DefectsList { get; set; }

        public async Task OnGetAsync()
        {
        }

        public async Task<IActionResult> OnPostSelectAsync()
        {
            // Check for valid defect number input
            if (Defect.DefectNo <= 0)
            {
                Message = new ResultMessage("Invalid defect number", true);

            }
            else
            {
                // Get the defect data
                var result = await _defectWrapper.GetByDefectNo(Defect.DefectNo);

                if (result.IsError)
                {
                    Message = new ResultMessage(result.Message, true);
                    Defect = new DefectModelVM { DefectNo = Defect.DefectNo };
                }
                else
                {
                    Message = new ResultMessage(result.Message, false);

                    Defect = new DefectModelVM
                    {
                        DefectNo = result.Defect.Defect,
                        ACNo = result.Defect.ACNo,
                        Station = result.Defect.Station,
                        FlightNo = result.Defect.Flight,
                        Date = DateTime.Parse(result.Defect.Reported_Date).ToString("yyyy-MM-dd"),
                        Description = result.Defect.Description,
                        Resolution = result.Defect.Resolution,
                    };
                }
            }

            return Page();
        }

        /// <summary>
        /// On insert
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostSaveAsync()
        {
            DefectModel defect = new DefectModel
            {
                Defect = Defect.DefectNo,
                ACNo = Defect.ACNo,
                Station = Defect.Station,
                Flight = Defect.FlightNo,
                Reported_Date = Defect.Date,
                Description = Defect.Description,
                Resolution = Defect.Resolution,

            };
            var response = await _defectWrapper.Add(defect);

            // Assign the view message
            Message = new ResultMessage(response.Message, response.IsError);

            return Page();
        }


        // Convert to JSON RPC IN THE VIEW WAY

        //public async Task<IActionResult> OnPostSaveAsync()
        //{
        //    DefectModel defect = new DefectModel
        //    {
        //        Defect = Defect.DefectNo,
        //        ACNo = Defect.ACNo,
        //        Station = Defect.Station,
        //        Flight = Defect.FlightNo,
        //        Reported_Date = Defect.Date,
        //        Description = Defect.Description,
        //        Resolution = Defect.Resolution,

        //    };

        //    // Create a model for json RPC
        //    var jsonRpcModel = new RPCRequestModel
        //    {
        //        Id = "1",
        //        Method = "insert",
        //        Params = JsonConvert.SerializeObject(defect) // Convert the defect data to json string
        //    };

        //    var response = await _rpc.Post(jsonRpcModel,"Defect");

        //    // Assign the view message
        //    Message = new ResultMessage(response.Message, response.IsError);
        //}
    }
}
