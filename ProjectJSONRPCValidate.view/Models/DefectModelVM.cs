﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectJSONRPCValidate.view.Models
{
    public class DefectModelVM
    {
        [Display(Name = "Defect No.")]
        public int DefectNo { get; set; }
        [Display(Name = "A/C No.")]
        public string ACNo { get; set; }
        [Display(Name = "Station")]
        public string Station { get; set; }
        [Display(Name = "Flight No.")]
        public string FlightNo { get; set; }
        [Display(Name = "Date/Time")]
        [DataType(DataType.Date)]
        public string Date { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Resolution")]
        public string Resolution { get; set; }

    }
}
