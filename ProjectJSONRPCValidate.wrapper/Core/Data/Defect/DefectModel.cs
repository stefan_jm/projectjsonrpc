﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectJSONRPCValidate.wrapper.Core.Data.Defect
{
    public class DefectModel
    {

        public int Defect { get; set; }

        public string ACNo { get; set; }

        public string Station { get; set; }

        public string Flight { get; set; }

        public string Reported_Date { get; set; }

        public string Description { get; set; }

        public string Resolution { get; set; }
    }
}
