﻿using ProjectJSONRPCValidate.wrapper.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectJSONRPCValidate.wrapper.Core.Data.Defect
{
    public interface IDefectWrapper
    {
        Task<ViewResponseModel> Add(DefectModel defectModel);
        //Task<ViewResponseModel> Update(DefectModel defectModel);
        Task<ViewResponseModel> Delete(DefectModel defectModel);
        Task<DefectResponseModel> GetByDefectNo(int defectNo);
    }
}
