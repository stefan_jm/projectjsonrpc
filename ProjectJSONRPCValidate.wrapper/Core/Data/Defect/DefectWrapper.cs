﻿using Newtonsoft.Json;
using ProjectJSONRPCValidate.wrapper.Core.Interfaces;
using ProjectJSONRPCValidate.wrapper.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectJSONRPCValidate.wrapper.Core.Data.Defect
{
    public class DefectWrapper : IDefectWrapper
    {
        private readonly IRPC _rpc;

        private static readonly string APIControllerName = "Defect";

        public DefectWrapper(IRPC rpc)
        {
            _rpc = rpc;
        }   

        public async Task<ViewResponseModel> Add(DefectModel defectModel)
        {
            // Create a model for json RPC
            var jsonRpcModel = new RPCRequestModel
            {
                Id = "1",
                Params = JsonConvert.SerializeObject(defectModel) // Convert the defect data to json string
            };
            // Check if already exists and should be updated
            // When there's an error - it does no exist so we will insert, otherwise we will update
            var checkDefect = await GetByDefectNo(defectModel.Defect);
            if (checkDefect.IsError)
                jsonRpcModel.Method = "insert";
            else
                jsonRpcModel.Method = "update";

            var response = await _rpc.Post(jsonRpcModel, APIControllerName);
            return ConvertToViewModel(response);
        }

        public async Task<ViewResponseModel> Delete(DefectModel defectModel)
        {
            var jsonRpcModel = new RPCRequestModel
            {
                Id = "2",
                Method = "delete",
                Params = JsonConvert.SerializeObject(defectModel)
            };

            var response = await _rpc.Post(jsonRpcModel, APIControllerName);
            return ConvertToViewModel(response);
        }

        public async Task<DefectResponseModel> GetByDefectNo(int defectNo)
        {
            var jsonRpcModel = new RPCRequestModel
            {
                Id = "2",
                Method = "select",
                Params = JsonConvert.SerializeObject(new Dictionary<string, object> { { "defect", defectNo } })
            };

            var defectResponseModel = new DefectResponseModel();

            var response = await _rpc.Post(jsonRpcModel, APIControllerName);
            // Check for error
            if (response.Error != null)
            {
                defectResponseModel.IsError = true;
                if (response.Error.Code != 0)
                    defectResponseModel.Message = string.Format("{0}: {1}", response.Error.Code, response.Error.Message);
                else
                    defectResponseModel.Message = response.Error.Message;
            }
            else
            {
                defectResponseModel.Defect = JsonConvert.DeserializeObject<DefectModel>(response.Result);
                defectResponseModel.Message = "Defect Exists";
            }

            return defectResponseModel;
        }


        /// <summary>
        /// Convert the JSON RPC response to a proper format for the view, taking into account if any errors occured.
        /// </summary>
        /// <param name="rpcResponse"></param>
        /// <returns></returns>
        private ViewResponseModel ConvertToViewModel(RPCResponseModel rpcResponse)
        {
            if (rpcResponse.Error != null)
            {
                var viewResponseModel = new ViewResponseModel { IsError = true };
                if (rpcResponse.Error.Code != 0)
                    rpcResponse.Error.Message = string.Format("{0}: {1}", rpcResponse.Error.Code, rpcResponse.Error.Message);
                else
                    rpcResponse.Error.Message = rpcResponse.Error.Message;

            }

            return new ViewResponseModel
            {
                Message = rpcResponse.Result
            };
        }
    }
}
