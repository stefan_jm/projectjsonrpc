﻿using Newtonsoft.Json;
using ProjectJSONRPCValidate.wrapper.Core.Interfaces;
using ProjectJSONRPCValidate.wrapper.Core.Models;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProjectJSONRPCValidate.wrapper.Core.Data
{
    public class RPC : IRPC
    {

        private readonly IHttpClientFactory _clientFactory;
        private readonly Uri APIUrl;

        public HttpClient Client { get; }

        public RPC(IHttpClientFactory clientFactory, string apiUrlPath)
        {
            APIUrl = new Uri(apiUrlPath);
            _clientFactory = clientFactory;
            Client = _clientFactory.CreateClient();
            Client.BaseAddress = APIUrl;
        }

        public async Task<RPCResponseModel> Get(RPCRequestModel data, string controllerName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Send json RPC data through http
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<RPCResponseModel> Post(RPCRequestModel data, string controllerName)
        {
            HttpContent content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
         
            var response = await Client.PostAsync(string.Format("{0}/{1}", controllerName, data.Method), content);

            // If the response failed because of the API and not because of the data sent then return a response object with the server error right away.
            if (!response.IsSuccessStatusCode)
                return new RPCResponseModel { Id = data.Id, Error = new RPCResponseModel.ErrorObject { Code = -1, Message = response.ReasonPhrase } };

            JsonSerializer serializer = new JsonSerializer();

            using (var contentStream = await response.Content.ReadAsStreamAsync())
            using (StreamReader sr = new StreamReader(contentStream))
            using (JsonReader reader = new JsonTextReader(sr))
            {
                return serializer.Deserialize<RPCResponseModel>(reader);
            }
        }

    }
}
