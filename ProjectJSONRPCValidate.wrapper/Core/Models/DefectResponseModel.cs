﻿using ProjectJSONRPCValidate.wrapper.Core.Data.Defect;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectJSONRPCValidate.wrapper.Core.Models
{
    public class DefectResponseModel : ViewResponseModel
    {
        public DefectModel Defect { get; set; }
    }
}
