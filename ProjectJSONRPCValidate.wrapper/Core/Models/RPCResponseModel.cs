﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectJSONRPCValidate.wrapper.Core.Models
{
    // Documentation and specifications from https://www.jsonrpc.org/specification 
    public class RPCResponseModel
    {

        /// <summary>
        /// A string specifying the version of the JSON-RPC Protocol. In our case 2.0 .
        /// </summary>
        public string JsonRPC { get; } = "2.0";

        /// <summary>
        /// This member is REQUIRED on success.
        /// It must not exist if there was an error invoking the method.
        /// The value of this member is determined by the method invoked on the Server.
        /// </summary>
        public string Result { get; set; } = null;

        /// <summary>
        /// This member is REQUIRED on error.
        /// It must no exist if there was no error triggered during the invocation.
        /// The value for this member must be an Object:
        /// When a rpc call encounters an error, the Response Object MUST contain the error member with a value that is an Object
        /// </summary>
        public ErrorObject Error { get; set; } = null;

        /// <summary>
        /// This member is REQUIRED.
        /// It must be the same as the value of the id member in the Request Model object.
        /// If there was an error in detecting the id in the Request object, the id must be NULL.
        /// </summary>
        public string Id { get; set; }

        public class ErrorObject
        {
            // The error codes from and including -32768 to -32000 are reserved for pre-defined errors. 
            // Any code within this range, but not defined explicitly below is reserved for future use. - see https://www.jsonrpc.org/specification 

            /// <summary>
            /// A number that indicates the error type that occured.
            /// It must be an integer
            /// </summary>
            public int Code { get; set; }

            /// <summary>
            /// A string providing a short description of the error.
            /// Should be limited to a single sentence.
            /// </summary>
            public string Message { get; set; }

            /// <summary>
            /// A primitive or Structured value that contains additional information about the error.
            /// This is not required and may be omitted / left out.
            /// The value of this member is defined by the Server (e.g. detailed error information, nested errors...).
            /// </summary>
            public string Data { get; set; }
        }
    }
}
