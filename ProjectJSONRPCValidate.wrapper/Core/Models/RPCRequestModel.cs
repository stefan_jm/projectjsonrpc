﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectJSONRPCValidate.wrapper.Core.Models
{
    // Documentation and specifications from https://www.jsonrpc.org/specification 
    public class RPCRequestModel
    {
        /// <summary>
        /// A string specifying the version of the JSON-RPC Protocol. In our case 2.0 .
        /// </summary>
        public string JsonRPC { get; } = "2.0";

        /// <summary>
        /// A string containing the name of the method to be invoked. Method names beginning with the word rpc followed by a period character are reserved
        /// for rpc-internal methods and extenions and MUST NOT be used for anything else.
        /// </summary>
        public string Method { get; set; }

        /// <summary>
        /// A structured value that holds the parameter values to be used during the invocation of the method. This may be omitted / left out.
        /// Parameters Structures: 
        ///     If present, parameters for the rpc call must be provided as a Structured value. Either by-position through an Array or by-name through an object.
        ///         * by-position: params MUST be an Array, containing the values in the Server expected order.
        ///         * by-name: params MUST be an Object, with member names that match the Server expected parameter names. 
        ///           The absence of expected names MAY result in an error being generated. The names MUST match exactly, including case, to the method's expected parameters.
        /// </summary>
        public string Params { get; set; }

        /// <summary>
        /// An identifier established by the client that must contain a String, Number, or NULL value if included. If it is not included it is assumed to be a notification.
        /// The value should normally not be NULL and Numbers should not contain fraction parts.
        /// </summary>
        public string Id { get; set; }

    }
}
