﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectJSONRPCValidate.wrapper.Core.Models
{
    /// <summary>
    /// Model class for sending response data to the View
    /// </summary>
    public class ViewResponseModel
    {

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="resultCode">Is the response an error is decided by the result code</param>
        //public ViewResponseModel(int resultCode)
        //{
        //    if (resultCode < 0)
        //        IsError = true;
        //}

        public string Message { get; set; }

        public bool IsError { get; set; } = false;
    }
}
