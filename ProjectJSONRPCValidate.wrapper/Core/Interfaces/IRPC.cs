﻿using ProjectJSONRPCValidate.wrapper.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectJSONRPCValidate.wrapper.Core.Interfaces
{
    public interface IRPC
    {
        Task<RPCResponseModel> Post(RPCRequestModel data, string controllerName);

        Task<RPCResponseModel> Get(RPCRequestModel data, string controllerName);
    }
}
