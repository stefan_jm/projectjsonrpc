﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectJSONRPCValidate.API.Core.Entities
{
    public class DefectEntity
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        public int Defect { get; set; }
        public string ACNo { get; set; }
        public string Station { get; set; }
        public string Flight { get; set; }
        public DateTime Reported_Date { get; set; }
        public string Description { get; set; }
        public string Resolution { get; set; }
    }
}
