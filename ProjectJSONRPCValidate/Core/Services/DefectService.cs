﻿using ProjectJSONRPCValidate.API.Core.Entities;
using ProjectJSONRPCValidate.API.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectJSONRPCValidate.API.Core.Services
{
    public class DefectService : IDefectService
    {

        private IDefectRepository _defectRepository;

        public DefectService(IDefectRepository defectRepository)
        {
            _defectRepository = defectRepository;
        }

        public async Task<bool> AddDefect(DefectEntity defect)
        {
            return await _defectRepository.Add(defect);
        }

        public async Task<bool> UpdateDefect(DefectEntity defect)
        {
            return await _defectRepository.Update(defect);
        }

        public async Task<List<DefectEntity>> GetAllDefects()
        {
            return await _defectRepository.GetAll();
        }

        public async Task<DefectEntity> GetDefectById(int defectNo)
        {
            return await _defectRepository.GetById(defectNo);
        }

        public async Task<List<DefectEntity>> GetDefectsPreview()
        {
            throw new NotImplementedException();
        }

    }
}
