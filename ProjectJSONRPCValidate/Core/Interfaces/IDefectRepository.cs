﻿using ProjectJSONRPCValidate.API.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectJSONRPCValidate.API.Core.Interfaces
{
    public interface IDefectRepository
    {
        Task<DefectEntity> GetById(int defectNo);
        Task<List<DefectEntity>> GetAll();
        Task<bool> Add(DefectEntity defect);
        Task<bool> Update(DefectEntity defect);
        Task<bool> Delete(DefectEntity defect);
    }
}
