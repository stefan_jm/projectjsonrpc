﻿using ProjectJSONRPCValidate.API.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectJSONRPCValidate.API.Core.Interfaces
{
    public interface IDefectService
    {
        Task<List<DefectEntity>> GetDefectsPreview();

        Task<List<DefectEntity>> GetAllDefects();

        Task<DefectEntity> GetDefectById(int defectNo);

        Task<bool> AddDefect(DefectEntity defect);

        Task<bool> UpdateDefect(DefectEntity defect);

    }
}
