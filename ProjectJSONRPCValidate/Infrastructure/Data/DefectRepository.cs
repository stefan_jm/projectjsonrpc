﻿using Dapper;
using ProjectJSONRPCValidate.API.Core.Entities;
using ProjectJSONRPCValidate.API.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectJSONRPCValidate.API.Infrastructure.Data
{
    public class DefectRepository : IDefectRepository
    {

        /// <summary>
        /// Connection to the DB
        /// </summary>  
        private readonly IDbConnection _dbConnection;


        public DefectRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public async Task<List<DefectEntity>> GetAll()
        {
            var query = "SELECT * FROM defects";

            var result = await _dbConnection.QueryAsync<DefectEntity>(query);

            return result.ToList();
        }

        public async Task<DefectEntity> GetById(int defectNo)
        {
            var query = "SELECT * FROM defects where defect=@defect";

            var parameters = new Dictionary<string, object> {
                { "@defect", defectNo }
            };

            var result = await _dbConnection.QuerySingleOrDefaultAsync<DefectEntity>(query, parameters);

            return result;
        }

        /// <summary>
        /// Add entity to database
        /// </summary>
        /// <param name="defect"></param>
        /// <returns>True if successfully added, false if not</returns>
        public async Task<bool> Add(DefectEntity defect)
        {
            var query = @"INSERT INTO defects(defect, acno, station, flight, reported_date, description, resolution) 
                            VALUES(@defect, @acno, @station, @flight, @reported_date, @description, @resolution)";

            var parameters = new Dictionary<string, object> {
                { "@defect", defect.Defect },
                { "@acno" , defect.ACNo },
                { "@station", defect.Station },
                { "@flight", defect.Flight },
                { "@reported_date", defect.Reported_Date},
                { "@description", defect.Description },
                { "@resolution", defect.Resolution }
            };

            var result = await _dbConnection.ExecuteAsync(query, parameters);

            if (result == 1)
                return true;
            else
                return false;


        }

        /// <summary>
        /// Update the entity
        /// </summary>
        /// <param name="defect"></param>
        /// <returns>True if successfully updated, false if not</returns>
        public async Task<bool> Update(DefectEntity defect)
        {
            var query = @"UPDATE defects
                            SET acno=@acno,
                                station=@station,
                                flight=@flight,
                                reported_date=@reported_date,
                                description=@description,
                                resolution=@resolution
                            WHERE defect=@defect";

            var parameters = new Dictionary<string, object> {
                { "@defect", defect.Defect },
                { "@acno" , defect.ACNo },
                { "@station", defect.Station },
                { "@flight", defect.Flight },
                { "@reported_date", defect.Reported_Date},
                { "@description", defect.Description },
                { "@resolution", defect.Resolution }
            };

            var result = await _dbConnection.ExecuteAsync(query, parameters);

            if (result == 1)
                return true;
            else
                return false;

        }

        /// <summary>
        /// Delete the entity
        /// </summary>
        /// <param name="defect"></param>
        /// <returns>True if successfully deleted, false if not</returns>
        public async Task<bool> Delete(DefectEntity defect)
        {
            var query = "DELETE FROM defects WHERE defect=@defect";

            var parameters = new Dictionary<string, object> { { "@defect", defect.Defect } };

            var result = await _dbConnection.ExecuteAsync(query, parameters);

            if (result == 1)
                return true;
            else
                return false;
        }
    }
}
