﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectJSONRPCValidate.API.Core.Entities;
using ProjectJSONRPCValidate.API.Core.Interfaces;
using ProjectJSONRPCValidate.API.Models;

namespace ProjectJSONRPCValidate.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class DefectController : ControllerBase
    {
        private readonly IDefectService _defectService;
        private readonly IDbConnection _dbConnection;

        public DefectController(IDefectService defectService, IDbConnection dbConnection)
        {
            _defectService = defectService;
            _dbConnection = dbConnection;
        }

        // Post Insert
        [HttpPost("insert")]
        public async Task<JsonResult> PostInsert([FromBody] RPCRequestModel sendModel)
        {
            var result = new RPCResponseModel { Id = sendModel.Id };

            // Check if sent data is empty, if not then insert to database
            if (string.IsNullOrEmpty(sendModel.Params))
            {
                result.Error = new RPCResponseModel.ErrorObject { Code = -32602, Message = "Empty body sent!" };
            }
            else
            {
                // Unserialize to object
                var defectModel = JsonConvert.DeserializeObject<DefectEntity>(sendModel.Params);
                if(defectModel == null)
                {
                    result.Error = new RPCResponseModel.ErrorObject { Code = -32602, Message = "Invalid data sent!" };
                }
                else
                {
                    if (await _defectService.AddDefect(defectModel))
                        result.Result = "Defect inserted successfully!";
                    else
                        result.Error = new RPCResponseModel.ErrorObject { Code = -32602, Message = "Error adding to database!" };
                }
            }

            return new JsonResult(result);
        }

        // Post Update
        [HttpPost("update")]
        public async Task<JsonResult> PostUpdate([FromBody] RPCRequestModel sendModel)
        {
            var result = new RPCResponseModel { Id = sendModel.Id };

            // Check if sent data is empty, if not then insert to database
            if (string.IsNullOrEmpty(sendModel.Params))
            {
                result.Error = new RPCResponseModel.ErrorObject { Code = -32602, Message = "Empty body sent!" };
            }
            else
            {
                // Unserialize to object
                var defectModel = JsonConvert.DeserializeObject<DefectEntity>(sendModel.Params);
                if (defectModel == null)
                {
                    result.Error = new RPCResponseModel.ErrorObject { Code = -32602, Message = "Invalid data sent!" };
                }
                else
                {
                    if (await _defectService.UpdateDefect(defectModel))
                        result.Result = "Defect updated successfully!";
                    else
                        result.Error = new RPCResponseModel.ErrorObject { Code = -32602, Message = "Error adding to database!" };
                }
            }

            return new JsonResult(result);


        }

        // Post select
        [HttpPost("select")]
        public async Task<JsonResult> PostSelect([FromBody] RPCRequestModel sendModel)
        {
            var result = new RPCResponseModel { Id = sendModel.Id };

            // Check if body is empty, POSSIBLY IF EMPTY THEN RETURN ALL DEFECTS instead of the ID that should be sent
            if (string.IsNullOrEmpty(sendModel.Params))
            {
                result.Error = new RPCResponseModel.ErrorObject { Code = -32602, Message = "Empty body sent!" };
            }
            else
            {
                // Unserialize to object
                var defectModel = JsonConvert.DeserializeObject<DefectEntity>(sendModel.Params);
                if (defectModel == null)
                {
                    result.Error = new RPCResponseModel.ErrorObject { Code = -32602, Message = "Invalid data sent!" };
                }
                else
                {
                    // Check for DB Connection - should redo it
                    if (!IsDbConnected())
                    {
                        result.Error = new RPCResponseModel.ErrorObject { Code = -32603, Message = "Database connection error!" };
                    }
                    else
                    {
                        var defect = await _defectService.GetDefectById(defectModel.Defect);
                        if (defect != null)
                            result.Result = JsonConvert.SerializeObject(defect);
                        else
                            result.Error = new RPCResponseModel.ErrorObject { Message = "Defect with given defect number does not exist!" };
                    }

                }
            }

            return new JsonResult(result);


        }

        private bool IsDbConnected()
        {
            try
            {
                _dbConnection.Open();
                _dbConnection.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public class RPCRequestModel
        {
            /// <summary>
            /// A string specifying the version of the JSON-RPC Protocol. In our case 2.0 .
            /// </summary>
            public string JsonRPC { get; } = "2.0";

            /// <summary>
            /// A string containing the name of the method to be invoked. Method names beginning with the word rpc followed by a period character are reserved
            /// for rpc-internal methods and extenions and MUST NOT be used for anything else.
            /// </summary>
            public string Method { get; set; }

            /// <summary>
            /// A structured value that holds the parameter values to be used during the invocation of the method. This may be omitted / left out.
            /// Parameters Structures: 
            ///     If present, parameters for the rpc call must be provided as a Structured value. Either by-position through an Array or by-name through an object.
            ///         * by-position: params MUST be an Array, containing the values in the Server expected order.
            ///         * by-name: params MUST be an Object, with member names that match the Server expected parameter names. 
            ///           The absence of expected names MAY result in an error being generated. The names MUST match exactly, including case, to the method's expected parameters.
            /// </summary>
            public string Params { get; set; }

            /// <summary>
            /// An identifier established by the client that must contain a String, Number, or NULL value if included. If it is not included it is assumed to be a notification.
            /// The value should normally not be NULL and Numbers should not contain fraction parts.
            /// </summary>
            public string Id { get; set; }

        }
    }


}