﻿using Moq;
using ProjectJSONRPCValidate.wrapper.Core.Data;
using ProjectJSONRPCValidate.wrapper.Core.Data.Defect;
using ProjectJSONRPCValidate.wrapper.Core.Interfaces;
using ProjectJSONRPCValidate.wrapper.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ProjectJSONRPCValidate.tests.wrapper.Defect.UnitTests
{
    public class DefectWrapperShould
    {
        private readonly string ControllerName = "Defect";

        private DefectWrapper _defectWrapper;

        private DefectModel TestDefectModel { get; set; }
        /// <summary>
        /// RPC Response model that is an error - IsError boolean is true
        /// </summary>
        private RPCResponseModel ErrorRPCResponseModel { get; set; }
        /// <summary>
        /// RPC Response model that is successful - IsError boolean is false
        /// </summary>
        private RPCResponseModel SuccessfulRPCResponseModel { get; set; }



        /// <summary>defectDoesNotExistErrorRPCResponseModel
        /// Setup class
        /// </summary>
        public DefectWrapperShould()
        {
            TestDefectModel = new DefectModel()
            {
                ACNo = "1",
                Defect = 1,
                Description = "decsrp",
                Flight = "flight1",
                Resolution = "resolution1",
                Station = "station1",
                Reported_Date = "reportedDate1"
            };

            SuccessfulRPCResponseModel = new RPCResponseModel()
            {
                Id = "1",
                Result = "{\"Defect\":1,\"ACNo\":\"1\",\"Station\":\"station1\",\"Flight\":\"flight1\",\"Reported_Date\":\"reportedDate1\",\"Description\":\"decsrp\",\"Resolution\":\"resolution1\"}"
            };

            ErrorRPCResponseModel = new RPCResponseModel()
            {
                Id = "1",
                Error = new RPCResponseModel.ErrorObject { Code = 1, Message = "error" }
            };
        }

        [Fact]
        public async void Add_NewDefectModel_ReturnsSuccessfulViewResponseModel()
        {
            // Arrange
            // setup mock
            var IRPCMock = new Mock<IRPC>();
            IRPCMock.Setup(x => x.Post(It.Is<RPCRequestModel>(e => e.Method == "select"), ControllerName)).ReturnsAsync(ErrorRPCResponseModel);
            IRPCMock.Setup(x => x.Post(It.Is<RPCRequestModel>(e => e.Method == "insert"), ControllerName)).ReturnsAsync(SuccessfulRPCResponseModel);

            _defectWrapper = new DefectWrapper(IRPCMock.Object);

            // Act
            var resultViewResponseModel = await _defectWrapper.Add(TestDefectModel);

            // Assert
            Assert.False(resultViewResponseModel.IsError);
        }

        [Fact]
        public async void Add_DefectModelWithIDThatExists_ReturnsSuccessfulViewResponseModel()
        {
            // Arrange
            // setup mock
            var IRPCMock = new Mock<IRPC>();
            IRPCMock.Setup(x => x.Post(It.Is<RPCRequestModel>(e => e.Method == "select"), ControllerName)).ReturnsAsync(SuccessfulRPCResponseModel);
            IRPCMock.Setup(x => x.Post(It.Is<RPCRequestModel>(e => e.Method == "update"), ControllerName)).ReturnsAsync(SuccessfulRPCResponseModel);
            _defectWrapper = new DefectWrapper(IRPCMock.Object);

            // Act
            var resultViewResponseModel = await _defectWrapper.Add(TestDefectModel);

            // Assert
            Assert.False(resultViewResponseModel.IsError);
        }

        [Fact]
        public async void GetByDefectNo_NotExistDefectModel_ReturnsIsErrorViewResponseModel()
        {
            // Arrange
            // setup mock
            var IRPCMock = new Mock<IRPC>();
            IRPCMock.Setup(x => x.Post(It.IsAny<RPCRequestModel>(), ControllerName)).ReturnsAsync(ErrorRPCResponseModel);
            _defectWrapper = new DefectWrapper(IRPCMock.Object);

            // Act
            var resultViewResponseModel = await _defectWrapper.GetByDefectNo(TestDefectModel.Defect);

            // Assert
            Assert.True(resultViewResponseModel.IsError);
        }

        [Fact]
        public async void GetByDefectNo_DefectExistsWithGivenId_ReturnsSuccessfulViewResponseModel()
        {
            // Arrange
            // setup mock
            var IRPCMock = new Mock<IRPC>();
            IRPCMock.Setup(x => x.Post(It.IsAny<RPCRequestModel>(), ControllerName)).ReturnsAsync(SuccessfulRPCResponseModel);
            _defectWrapper = new DefectWrapper(IRPCMock.Object);

            // Act
            var resultViewResponseModel = await _defectWrapper.GetByDefectNo(TestDefectModel.Defect);

            // Assert
            Assert.False(resultViewResponseModel.IsError);
        }
    }
}
